# myChat

Angular 5 / Ionic 3 / SendBird messaging application


## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development.


### Prerequisites

Be sure to have Node.js and npm installed on your machine:  
https://nodejs.org/en/

If you want to export the application as an .apk to run it on your Android mobile:   
The Java jdk:  
http://www.oracle.com/technetwork/java/javase/downloads/index.html

And the Android sdk:  
https://developer.android.com/studio/index.html#downloads


### Installing

Install Apache Cordova:  
$ sudo npm install -g cordova

Install Ionic:  
$ sudo npm install -g ionic

Get the projet from Gitlab:  
$ git clone https://gitlab.com/aurelien.jule.dev/technical-assignment/

Install the node dependencies:  
$ npm install


## Running and debugging

To launch the project in your browser, run the following command in the myChat folder:  
$ ionic serve

To debug the project in a browser, I recommand Visual Studio Code and the VS Code extension: "Debugger for Chrome".  
There is a launch.json file in the .vscode folder that can be used to launch and debug the application.  
https://code.visualstudio.com/docs/editor/debugging

To launch the application on your phone, be sure to let it in USB debugging mode and run:  
$ ionic cordova run android

To export the application as an .apk, run:  
$ ionic cordova build android


## Authors

Aurélien Julé

## License

This project is free to use, copy, share, edit...