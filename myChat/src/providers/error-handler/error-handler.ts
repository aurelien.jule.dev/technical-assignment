import { Injectable } from '@angular/core';
import { AlertController } from 'ionic-angular';

@Injectable()
export class ErrorHandlerProvider {

  constructor(private alertCtrl: AlertController) {
  }

  displayAndLogError(error) {
    console.log(error);
    const alert = this.alertCtrl.create({
      title: 'Erreur:',
      message: error.message,
      buttons: ['Ok']
    });
    alert.present();
    return;
  }
}
