import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable()
export class CryptosProvider {

  constructor(private http: HttpClient) {
  }

  getCurrentCryptoPrice() {
    return this.http.get('https://api.coindesk.com/v1/bpi/currentprice.json');
  }
}
