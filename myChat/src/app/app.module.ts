import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { HttpClientModule } from '@angular/common/http';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { CryptosProvider } from '../providers/cryptos/cryptos';
import { ErrorHandlerProvider } from '../providers/error-handler/error-handler';
import { SanitizerPipe } from '../pipes/sanitizer/sanitizer';

@NgModule({
  declarations: [
    MyApp,
    HomePage,
    LoginPage,
    SanitizerPipe
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    LoginPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    CryptosProvider,
    HttpClientModule,
    ErrorHandlerProvider
  ]
})
export class AppModule {}
