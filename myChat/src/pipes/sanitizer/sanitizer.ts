import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
  name: 'sanitizerPipe'
})
export class SanitizerPipe implements PipeTransform {

  constructor(private sanitizer: DomSanitizer) {

  }

  transform(url) {
    url = url.replace('https://www.youtube.com/watch?v=', 'https://www.youtube.com/embed/');
    console.log(url);
    return this.sanitizer.bypassSecurityTrustResourceUrl(url);
  }
}
