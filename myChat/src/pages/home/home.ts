import { Component, ViewChild } from '@angular/core';
import { NavParams, Content, LoadingController } from 'ionic-angular';
import { CryptosProvider } from '../../providers/cryptos/cryptos'

import SendBird from 'sendbird';
import { NgForm } from '@angular/forms';

import * as config from '../../../config/dev.json';
import { ErrorHandlerProvider } from '../../providers/error-handler/error-handler';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild(Content) content: Content;

  message: string = '';
  private nickName: string;
  private sb = new SendBird({ 'appId': config[0].sendbird_appId });
  private channel: SendBird.OpenChannel;
  private channelHandler = new this.sb.ChannelHandler();
  private messageList: any;
  private messageListQuery: SendBird.PreviousMessageListQuery;
  private loading = this.loadingCtrl.create({
    content: 'Please wait...'
  });

  constructor(
    private navParams: NavParams,
    private loadingCtrl: LoadingController,
    private cryptosProvider: CryptosProvider,
    private errorHandler: ErrorHandlerProvider
  ) {
    this.loading.present();
    this.nickName = this.navParams.get('nickName');
    this.sb.connect(this.nickName, (user, error) => {
      error ? this.errorHandler.displayAndLogError(error) : false;
      this.sb.OpenChannel.getChannel(config[0].sendbird_openChannel, (channel, error) => {
        error ? this.errorHandler.displayAndLogError(error) : false;
        channel.enter((response, error) => {
          error ? this.errorHandler.displayAndLogError(error) : false;
          this.channel = channel;
          this.messageListQuery = this.channel.createPreviousMessageListQuery();
          this.loadFirstMessages();
        });
      });
    });

    this.channelHandler.onMessageReceived = (channel, message) => {
      this.messageList.push(message);
      setTimeout(() => {
        this.content.scrollToBottom(300);
      }, 50);
    };
    this.sb.addChannelHandler('1', this.channelHandler);
  }

  sendMessage(form: NgForm) {
    this.displayCurrentCryptoPrice(form.value.message).then(
      message => {
        this.channel.sendUserMessage(message as string, (message, error) => {
          error ? this.errorHandler.displayAndLogError(error) : false;
          this.messageList.push(message);
          setTimeout(() => {
            this.content.scrollToBottom(300);
          }, 50);
        });
        this.message = '';
      }
    )
  }

  loadFirstMessages() {
    this.messageListQuery.load(25, true, (messageList, error) => {
      error ? this.errorHandler.displayAndLogError(error) : false;
      if (messageList) {
        this.messageList = messageList.reverse();
      }
      setTimeout(() => {
        this.content.scrollToBottom(300);
        this.loading.dismiss();
      }, 50);
    });
  }

  loadMessagesOnScroll(event) {
    this.messageListQuery.load(25, true, (messageList, error) => {
      error ? this.errorHandler.displayAndLogError(error) : false;
      if (messageList) {
        this.messageList = messageList.reverse().concat(this.messageList);
      }
      event.complete();
    });
  }

  displayCurrentCryptoPrice(message: string) {
    return new Promise(resolve => {
      if (message === '!bitcoin') {
        this.cryptosProvider.getCurrentCryptoPrice().subscribe(data => {
          resolve('!bitcoin: The current bitcoin price is ' + data['bpi']['USD']['rate'] + '$');
        }, error => {
          this.errorHandler.displayAndLogError(error);
        });
      } else {
        resolve(message);
      }
    });
  }
}
